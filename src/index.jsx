// @flow
import React from 'react'
import { render } from 'react-dom'
import {
  HashRouter as Router,
  Route,
  Switch,
} from 'react-router-dom'

import 'normalize.css'
import './style.sss'

import { Provider } from './actions'
import Home from './containers/Home'
import Item from './containers/Item'


const Init = Provider(() => (
  <Router>
    <div>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/item/:id" component={Item} />
      </Switch>
    </div>
  </Router>
))

render(<Init />, document.getElementById('main'))
