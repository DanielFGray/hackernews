// @flow
const has = (o: Object, k: string): boolean =>
  Object.prototype.hasOwnProperty.call(o, k)

const merge = (...a: Array<Object>): Object =>
  Object.assign({}, ...a)

export { has, merge }
