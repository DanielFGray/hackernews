// @flow

export type CommentType = {
  by: string,
  kids: Array<number>,
  text: string,
  time: number,
}

export type Post = {
  by: string,
  kids?: Array<number>,
  descendants: number,
  id: number,
  text?: string,
  time: number,
  title: string,
  url?: string,
}
