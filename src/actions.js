// @flow
import { provideState } from 'freactal'
import { Observable } from 'rxjs/Observable'
import 'rxjs/add/observable/from'
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/mergeMap'
import 'rxjs/add/operator/filter'
import 'rxjs/add/operator/take'
import 'rxjs/add/operator/toPromise'
import { memoize } from 'lodash/fp'
import { has } from './utils'

const wrapWithPending = (pendingKey, cb) => (effects, ...a) =>
  effects.setFlag(pendingKey, true)
    .then(() => cb(effects, ...a))
    .then(value => effects.setFlag(pendingKey, false).then(() => value))

const HN = memoize((data: string) =>
  Observable.from(fetch(`https://hacker-news.firebaseio.com/v0/${data}.json`).then(x => x.json()))
    .map(x => x))

const getUser = user => HN(`user/${user}`)
const getPost = id => HN(`item/${id}`)

const Provider = provideState({
  initialState: () => ({
    newsList: [],
    newsPending: false,
    filterText: '',
    userInfo: [],
  }),
  effects: {
    setFlag: (effects, key, value) => state => ({ ...state, [key]: value }),
    addNewsItem: (_, item) => state => ({ ...state, newsList: state.newsList.concat(item) }),
    getNews: wrapWithPending('newsPending', effects =>
      HN('topstories')
        .flatMap(x => x)
        .take(50) // FIXME: takewhile
        .flatMap(id => getPost(id))
        .filter(x => x.type === 'story')
        .map(effects.addNewsItem)
        .toPromise()),
    filterChange: (_, str) => state => ({ ...state, filterText: str }),
    getUser: (_, user) => state => ({ ...state, userInfo: state.userInfo.concat(getUser(user)) }),
  },
  computed: {
    filteredList: ({ newsList, filterText }) =>
      newsList.filter(e =>
        has(e, 'title') &&
          e.title.toLowerCase().includes(filterText.toLowerCase())),
    getPost: ({ newsList }, id) => newsList.find(e => e.id === id),
  },
})

export { Provider, HN }
