// @flow
import React, { Component } from 'react'
import { injectState } from 'freactal'

import { has } from '../utils'
import { HN } from '../actions'
import ListItem from '../components/ListItem'
import CommentList from '../components/CommentList'
import type { Post } from '../types'

class Item extends Component {
  props: {
    match: {
      params: {
        id: string,
      }
    },
    state: {
      newsList: Array<Object>,
    },
  }
  state: { post: Post }

  state = {
    post: {
      by: '',
      descendants: 0,
      id: 0,
      text: '',
      time: 0,
      title: '',
      url: '',
      kids: [],
    },
  }

  componentWillMount() {
    const cache = this.props.state.newsList
      .find(e => e.id === Number(this.props.match.params.id))
    if (cache) {
      this.setState({ post: cache })
    } else {
      HN(`item/${this.props.match.params.id}`)
        .subscribe(post => this.setState({ post }))
    }
  }

  render() {
    return (
      <div>
        {has(this.state.post, 'id') && <ListItem {...this.state.post} />}
        {has(this.state.post, 'kids') &&
          this.state.post.kids.map(e => <CommentList id={e} key={e} />)}
      </div>
    )
  }
}

export default injectState(Item)
