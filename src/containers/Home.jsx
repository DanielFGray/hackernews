// @flow
import React, { Component } from 'react'
import { injectState } from 'freactal'

import ListItem from '../components/ListItem'
import Loading from '../components/Loading'
import style from '../style.sss'

class Home extends Component {
  props: {
    effects: {
      getNews: Function,
      filterChange: Function,
    },
    state: {
      newsPending: boolean,
      newsList: Array<Object>,
      filteredList: Array<Object>,
      filterText: string,
    },
  }

  componentWillMount() {
    if (this.props.state.newsList.length < 1) {
      this.props.effects.getNews()
    }
  }

  filterChange = (e: SyntheticInputEvent) => {
    this.props.effects.filterChange(e.target.value)
  }

  render() {
    const {
      newsPending,
      newsList,
      filteredList,
      filterText,
    } = this.props.state
    return (
      <div className={style.newsList}>
        <div className={style.filter}>
          <input
            type="text"
            onChange={this.filterChange}
            placeholder="search"
          />
        </div>
        <div>
          {filterText.length > 0
            ? filteredList.map(e => <ListItem key={e.id} {...e} />)
            : newsList.length > 0 && newsList.map(e => <ListItem key={e.id} {...e} />)}
          {newsPending && <Loading />}
        </div>
      </div>
    )
  }
}

export default injectState(Home)
