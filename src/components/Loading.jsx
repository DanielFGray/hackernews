// @flow
import React from 'react'

import style from '../style.sss'

const Loader = () => (
  <div
    style={{
      display: 'flex',
      justifyContent: 'space-around',
      alignItems: 'center',
    }}
  >
    <div className={style.loading} />
  </div>
)

export default Loader
