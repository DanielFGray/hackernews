// @flow
import React, { PureComponent } from 'react'
import { Link } from 'react-router-dom'
import ago from 's-ago'

import type { Post } from '../types'
import style from '../style.sss'

class ListItem extends PureComponent {
  props: Post

  render() {
    return (
      <div className={style.listItem}>
        <div>
          {this.props.text
            ? <Link to={`/${this.props.id}`}>{this.props.title}</Link>
            : <a href={this.props.url} target="_blank" rel="noopener noreferrer">
              {this.props.title}
            </a>}
        </div>
        <div style={{ fontSize: 'smaller' }}>
          <span>by {this.props.by}</span>
          <span> - {ago(new Date(this.props.time * 1000))}</span>
          <span> | <Link to={`/item/${this.props.id}`}>{this.props.descendants}</Link></span>
        </div>
      </div>
    )
  }
}

export default ListItem
