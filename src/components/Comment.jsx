// @flow
import React, { Component } from 'react'
import ago from 's-ago'

import CommentList from './CommentList'
import type { CommentType } from '../types'
import style from '../style.sss'

class Comment extends Component {
  props: CommentType

  static defaultProps = {
    by: '',
    kids: [],
    text: '',
    time: 0,
  }

  render() {
    const { by, kids, text, time } = this.props
    return (
      <div className={style.card}>
        <div>{by} {ago(new Date(time * 1000))}</div>
        <div dangerouslySetInnerHTML={{ __html: text }} />
        {kids && kids.map(e => <CommentList id={e} key={e} />)}
      </div>
    )
  }
}

export default Comment
