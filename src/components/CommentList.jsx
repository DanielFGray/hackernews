// @flow
import React, { Component } from 'react'

import { HN } from '../actions'
import Loading from './Loading'
import Comment from './Comment'
import type { CommentType } from '../types'

class CommentList extends Component {
  props: {
    id: number,
  }
  state: {
    data: CommentType,
  }
  subscription: { unsubscribe: Function }

  state = {
    data: {},
  }

  componentDidMount() {
    this.subscription = HN(`item/${this.props.id}`)
      .subscribe(data => this.setState({ data }))
  }

  componentWillUnmount() {
    this.subscription.unsubscribe()
  }

  render() {
    return (
      <div>
        {Object.keys(this.state.data).length > 0 ? <Comment {...this.state.data} /> : <Loading />}
      </div>
    )
  }
}

export default CommentList
